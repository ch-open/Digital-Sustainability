# Digital Sustainability – Workshop mit STI & CH Open

- Workshop: Digital Sustainability
- Language (Written / Spoken): englisch / english | german
- Date & Time: 9.9., 16h00-17h30 
- Location: Online, BigBlueButton
- Recording: yes
- Target: 30-50 participants

technical notes: make use of digital sustainable tools and techniques

## Speaker
- Dr. Matthias Stürmer: Präsident CH Open & Professor BFH
- @MarkusTiede: Vize-Präsident CH Open & Software-Engineer Basler Versicherung

## Abstract
DE: Digitalisierung und Nachhaltigkeit sind allgegenwärtige Konzepte. Doch wie lassen sich diese gemeinsam denken? Die Antwort erscheint trivial: Digitale Nachhaltigkeit. Was genau unter digitaler Nachhaltigkeit verstanden wird und welche Voraussetzungen dafür nötig sind, wird dir während dem Workshop “Digitale Nachhaltigkeit” vermittelt und anhand des Praxisbeispiels der Basler Versicherung veranschaulicht. 

EN: Digitalization and sustainability are omnipresent concepts. But how can they be thought of together? The answer seems trivial: digital sustainability. What exactly digital sustainability means, what it encompasses and what prerequisites are necessary for it will be explained to you during the workshop "Digital Sustainability" and illustrated with a use case at Basler Versicherung (s. [1]).

[1] https://www.baloise.com/de/home/ueber-uns/wofuer-wir-stehen/nachhaltigkeit/it-security.html
[2] https://github.com/baloise/open-source

## Objectives
Vermittlung des Konzepts digitaler Nachhaltigkeit
Voraussetzung digitaler Nachhaltigkeit
Veranschaulichung an einem Praxisbeispiel
Fragen und Antwort

## Agenda 
- Welcome, introduction and check-in by the STI Team & CH Open/Parldigi Team (10’) (Alessa & Jannis)
- Expert input (20’) (Matthias)
- [Company case study (20’)](https://github.com/baloise/open-source/blob/main/docs/slides/ws-digital-sustain/content.md)
- Interactive Peer-to-Peer exchange (30’) (Markus & Matthias, …) (Breakout Rooms zu den beiden Inputs) (Rotation between rooms; if many people 3rd room STI & Digital Sustainability) → Rotation nach 15 Minuten (Raum 1 gehen in Raum 2) WICHTIG: Braucht Moderatoren pro Raum (Alessa und Jannis)
- Sammeln wichtigster Learnings, 3-4 Statements
- Check-out and Call to action by STI Team & CH Open Team (10’) (Blab & Jannis): DINAcon und STI Anmeldung

Social Media Postings:
[9.9.21 | Workshop Digital Sustainability together with @STI and @Blab | online]
Digitalization and sustainability are omnipresent concepts. But how can they be thought of together? The answer seems trivial: digital sustainability. What exactly digital sustainability means, what it encompasses, and what prerequisites are necessary for it will be explained to you during the workshop "Digital Sustainability" and illustrated with a use case at Basler Versicherung.

## CoWorking-Board

Link to the Excalidraw: https://excalidraw.com/#room=753fe7d3a953061469d5,EdSoi-d5ywMEZU2dPPvOng
