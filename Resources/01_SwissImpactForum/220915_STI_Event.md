| **Title** : How to implement Sustainability in your Digital Transformation Journey |
| --- |
| **Co-Hosts:** Canopé, CH Open; to be confirmed: Digital Switzerland, BEKB, HUG (Hôpitaux universitaires de Genève) |
| **Abstract** : Did you ever wonder how to combine digitalization and sustainability, two omnipresent concepts? Are you looking for practical tips and information to implement it quickly and efficiently? Then, this workshop is for you. You will learn from the &quot;Collective for sustainable digital transformation&quot; (Canopé and CH Open) why and how to implement a sustainable IT approach within your digital transformation. Additionally, you will gain insights into best practices from Digital Switzerland, BEKB and HUG (Hôpitaux universitaires de Genève). |

**Open Tasks:** 

1. **Create Quiz Questions**

Questions for sustainable digitalization:

- How many smartphones are sold every second? 40
- In 2018, how much CO2 was emitted by online videos? As much as Spain
- What % of global data traffic comes from online videos? 70%
- What percentage of smartphones are recycled worldwide? 20%
- How many natural resources are needed to manufacture a smartphone? 2&#39;800 kg

**Goal:** Have 3-5 question that show the relevancy of digital sustainability

2. **Create Pledges**

Existing Pledges [Canopé](https://canope.net/) (not yet iSMART)

- Engage management with real sustainable IT objectives and means (human &amp; budget dedication)
- Elaborate a Sustainable IT strategy,
- Raise awareness among your employees,
- Measure your information system footprint,
- Develop your website and digital applications with a sustainable way (how to translate &quot;eco-conception&quot;?).

**Goals:**

1) Most Non-Software developers will take measures for sustainable digitization under the pledge of becoming a net zero company. So it would be helpful to suggest measures to include measures for sustainable digitalization that help them reduce emissions.

2) For Software Developers, we would like to have a few suggestions on pledges they can take to increase their digital sustainability and support the sustainable digital transformation as a whole with concrete measures.

Pledges need to be… iSMART:

- Impact
- Specific
- Measurable
- Attainable
- Relevant
- Time-bound
